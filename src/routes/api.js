const express = require ('express');
const router = express.Router();
const locationController = require('../controllers/locationController');
const userController = require('../controllers/userController');
const stationController = require('../controllers/stationController');

router.get('/location', locationController.list);
router.post('/location', locationController.save);
router.put('/location', locationController.update);
router.delete('/location', locationController.delete);
router.get('/location/:id/stations', locationController.getStations);
router.get('/location/:id', locationController.get);
router.get('/location/:id/water-consumed', locationController.getTotalWaterConsumed);



router.get('/station', stationController.list);
router.post('/station', stationController.save);
router.put('/station', stationController.update);
router.delete('/station', stationController.delete);


router.post('/login', userController.login);
module.exports = router;