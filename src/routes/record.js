const express = require ('express');
const router = express.Router();

const recordcontroller = require('../controllers/recordcontroller.js');

router.get('/', recordcontroller.list);
router.post('/add', recordcontroller.save);
router.get('/delete/id:', recordcontroller.delete);

router.get('/update/id:', recordcontroller.edit);
router.post('/update/id:', recordcontroller.update);
module.exports = router;    