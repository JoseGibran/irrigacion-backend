const express = require ('express');
const router = express.Router();

const stationcontroller = require('../controllers/stationcontroller');

router.get('/', stationcontroller.list);
router.post('/add', stationcontroller.save);
router.get('/delete/id:', stationcontroller.delete);

router.get('/update/id:', stationcontroller.edit);
router.post('/update/id:', stationcontroller.update);
module.exports = router;    