const express = require ('express');
const router = express.Router();

const water_recordcontroller = require('../controllers/water_recordcontroller');

router.get('/', water_recordcontroller.list);
router.post('/add', water_recordcontroller.save);
router.get('/delete/id:', water_recordcontroller.delete);

router.get('/update/id:', water_recordcontroller.edit);
router.post('/update/id:', water_recordcontroller.update);
module.exports = router;    