const express = require ('express');
const router = express.Router();

const authcontroller = require('../controllers/authcontroller');

router.get('/', authcontroller.list);
router.post('/add', authcontroller.save);
router.get('/delete/id:', authcontroller.delete);

router.get('/update/id:', authcontroller.edit);
router.post('/update/id:', authcontroller.update);
module.exports = router;    