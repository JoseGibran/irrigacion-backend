const express = require ('express');
const router = express.Router();

const locationcontroller = require('../controllers/locationcontroller');

router.get('/', locationcontroller.list);
router.post('/add', locationcontroller.save);
router.get('/delete/id:', locationcontroller.delete);

router.get('/update/id:', locationcontroller.edit);
router.post('/update/id:', locationcontroller.update);
module.exports = router;    