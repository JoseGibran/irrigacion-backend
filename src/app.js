const express = require('express');
const path = require ('path');
const cors = require('cors');
const sql = require('mssql');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer();
const app = express ();

const api = require('./routes/api');

//DB Connection
const init = async () => {
  try {
      await sql.connect('mssql://joselyne:123@localhost/irrigacion2')
      
      // ajustes
      app.set('port', process.env.PORT || 8082);

      //middlewares
      app.use(express.urlencoded({extended:false}));
      app.use(cors());
      app.use((req, res, next)=> {
        req.sql = sql;
        next();
      });
      app.use(bodyParser.json());
      app.use(bodyParser.urlencoded({ extended: true })); 
      app.use(upload.array()); 
      app.use(express.static('public'));
      //routes
      app.use('/api', api);

      app.listen(8082, () => {
          console.log('server on port 8082');
      }); 

  } catch (err) {
    console.log(err, "DB ERR");
  }
}
init();


