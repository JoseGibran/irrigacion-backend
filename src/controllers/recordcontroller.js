const controller = {};

controller.list = (req, res) => {
       req.getConnection((err,conn) =>{
           conn.query('SELECT * FROM Record', (err, records) => {
               if(err){
                   res.json(err);
               }
               console.log(records);
               data: records
           });
       });
};

controller.save = (req, res) =>{
    const data= req.body;

    req.getConnection((err, conn) => {
        conn.query('INSERT INTO Record set ?', [data], (err, rows) =>{ 
            res.redirec('/');        
        });
    })
};

controller.edit = (req, res) =>{
    const { id } = req.params;
    req.getConnection8((err,conn) =>{
        conn.query('SELECT * FROM Record WHERE id = ?', [id], (err, record) =>{
            res.render('record_edit', {
                data: record[0]
            });
        });
    });
};

controller.update = (req, res) =>{
    const { id } = req.params;
    const newrecord= req.body;

    req.getConnection((err, conn) =>{
        conn.query('UPDATE Record ser ? WHERE id = ?', [newrecord, id], (err, rows) => {
            res.redirec('/');
        });
    });
};

controller.delete = (req, res) =>{
    const { id } = req.params.id;
    req.getConnection((err,conn) =>{
        conn.query('DELETE FROM Record WHERE id = ?', [id], (err, rows) =>{
            res.redirec('/');
        });
    })
    console.log(req.params.id);
};


module.export = controller;