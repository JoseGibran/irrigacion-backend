const controller = {};

controller.list = async (req, res) => {
    try{
        const result = await req.sql.query`select * from Location`;
        const data =  result.recordsets[0].map(location => {
            return {
                ...location,
                polygon: JSON.parse(location.polygon)
            }
        });
        res.json({success: true, data});
    }catch(e){
        res.json({success: false, message: e.message});
    } 
};


controller.get = async (req, res) => {
    try{
        const result = await req.sql.query`SELECT * FROM Location WHERE id=${req.params.id}`;
        const data = result.recordsets[0][0];
        res.json({
            data,
            success: true
        });
    }catch(e){
        res.json({success: false, message: e.message});
    }
}

controller.getStations = async (req, res) => {
    try{
        const stationsResult = await req.sql.query`SELECT Station.id, Station.lat, Station.lng, Station.is_water_open FROM Location 
        INNER JOIN Station ON Location.id = Station.location_id 
        WHERE Location.id = ${req.params.id}
        `;

        const stationWithParamsResult = await req.sql.query`
            SELECT Record.station_id, Record.humidity, Record.luminosity, Record.temperature, Record.captured_at FROM Record
            INNER JOIN Station on Record.station_id = Station.id
            WHERE Station.location_id = ${req.params.id} AND DATEADD(MINUTE,2, Record.captured_at) >= CURRENT_TIMESTAMP
            ORDER BY captured_at DESC 
        `;

        let data = [];

        stationsResult.recordsets[0].forEach( async (x, index) => {
            let tempParams = stationWithParamsResult.recordsets[0].find(y =>y.station_id === x.id);
            if(!tempParams){
                const recordResult = await req.sql.query`SELECT TOP 1 * FROM Record WHERE station_id = ${x.id} ORDER BY id DESC`;
                tempParams = recordResult.recordsets[0][0] || {};
            }
            data = [...data, {
                id: x.id,
                lat: x.lat,
                lng: x.lng,
                is_water_open: x.is_water_open,
                humidity: tempParams.humidity,
                luminosity: tempParams.luminosity,
                temperature: tempParams.temperature,
            }];
            
            if(data.length === stationsResult.recordsets[0].length ){
                res.json({
                    data,
                    success: true
                });
            }
        });
        
        
    }catch(e){
        res.json({success: false, message: e.message});
    }
}

controller.getTotalWaterConsumed = async (req, res) => {
    try {
        const result = await req.sql.query`
            SELECT l.id, SUM(DATEDIFF(SECOND, w.started_at, w.ended_at)) AS 'open_time', l.water_pressure FROM Water_record w
            INNER JOIN Station s ON w.station_id = s.id
            INNER JOIN Location l ON s.location_id = l.id
            WHERE l.id = ${req.params.id} AND w.ended_at IS NOT NULL
            GROUP BY l.id, l.water_pressure
        `;

        res.json({
            success: true,
            data: result.recordsets[0][0],
        });

    }catch(e){
        res.json({success: false, message: e.message});
    }
}

controller.save = async (req, res) =>{
    const data = req.body;
    const {name, polygon, water_pressure} = data;
    try{

        const result = await req.sql.query(`INSERT INTO Location VALUES ('${name}', '${polygon}', ${water_pressure} )`, (err, result) => {
            if(err) return res.json({success: false, message: "Error on insert location."});
            
            if(result.rowsAffected[0]){
                res.json({success: true, data: {...req.body}});
            }else{
                res.json({success: false, message: "Error on insert location."});
            }
        });
    }catch(e){
        res.json({success: false, message: e.message});
    }
};

controller.update = async (req, res) =>{
    const data= req.body;
    const {name, polygon, water_pressure, id} = data;
    try{
        const result = await req.sql.query`UPDATE Location SET name='${name}', polygon='${polygon}', water_pressure=${water_pressure} WHERE id=${id}`;
        if(result.rowsAffected[0]){
            res.json({success: true, data: {...req.body}});
        }else{
            res.json({success: false, message: "Error on update location."});
        }
        
    }catch(e){
        res.json({success: false, message: e.message});
    }
};


controller.delete = async (req, res) =>{
    const data= req.body;
    const {id} = data;
    try{
        const result = await req.sql.query`DELETE FROM Location  WHERE id=${id}`;
        if(result.rowsAffected[0]){
            res.json({success: true, data: {...req.body}});
        }else{
            res.json({success: false, message: "Error on delete location."});
        }
        
    }catch(e){
        res.json({success: false, message: e.message});
    }
};


module.exports = controller;