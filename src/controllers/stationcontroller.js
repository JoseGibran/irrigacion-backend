const controller = {};

controller.list = async (req, res) => {
    try{
        const result = await req.sql.query`select * from Station`;
        const data =  result.recordsets[0].map(station => {
            return {    
                ...station,
                lng: JSON.parse(station.lng)
            }
        });
        res.json({success: true, data});
    }catch(e){
        res.json({success: false, message: e.message});
    }
       
};

controller.save = async (req, res) =>{
    const data = req.body;
    const {lat, lng, location_id, is_water_open} = data;
    try{

        req.sql.query(`INSERT INTO Station VALUES ('${lat}', '${lng}', '${location_id}', ${is_water_open} )`, (err, result) => {
            if(err) res.json({success: false, message: "Error on insert station."});
            
            if(result.rowsAffected[0]){
                res.json({success: true, data: {...req.body}});
            }else{
                res.json({success: false, message: "Error on insert station."});
            }
        });
    }catch(e){
        res.json({success: false, message: e.message});
    }
};

controller.update = async (req, res) =>{
    const data = req.body;
    const {lat, lng, is_water_open, id} = data;
    try{

        req.sql.query(`UPDATE Station SET lat='${lat}', lng='${lng}', is_water_open='${is_water_open}' WHERE id=${id}`, (err, result) => {
            if(err) res.json({success: false, message: "Error on update station."});
            
            if(result.rowsAffected[0]){
                res.json({success: true, data: {...req.body}});
            }else{
                res.json({success: false, message: "Error on update station."});
            }
        });
    }catch(e){
        res.json({success: false, message: e.message});
    }
};

controller.delete = async (req, res) =>{
    const data = req.body;
    const {id} = data;
    try{

        req.sql.query(`DELETE FROM Station  WHERE id=${id}`, (err, result) => {
            if(err) res.json({success: false, message: "Error on delete station."});
            
            if(result.rowsAffected[0]){
                res.json({success: true, data: {...req.body}});
            }else{
                res.json({success: false, message: "Error on delete station."});
            }
        });
    }catch(e){
        res.json({success: false, message: e.message});
    }
};

module.exports = controller;