const jwt = require('jsonwebtoken');
const controller = {};
const jwtKey = 'test';



controller.login = async (req, res) => {
    const data= req.body;
    const {username, password} = data;
    try{
        const result = await req.sql.query`select * from Auth WHERE username=${username}`;
        const user = result.recordsets[0][0];
        
        if(user){
            if(result.recordsets[0][0].password == password){
                res.json({success: true, data: {
                    id: user.id,
                    username: user.username,
                    token: jwt.sign({ id: user.id, username: user.username }, '123')
                }});
            }else{
                res.json({success: false, message: 'User or pass invalid'});
            }
        }else{
            res.json({success: false, message: 'User not found'});
        }
    }catch(e){
        res.json({success: false, message: e.message});
    }
       
};

module.exports = controller;