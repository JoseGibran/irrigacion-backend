const sql = require('mssql');
const fetch = require("node-fetch");
const weather_appid = '532263880bb70a5b21538488461506c4';
const moment = require('moment-timezone');
var geoTz = require('geo-tz');

const getWeatherFactor = (weather) => {
    if(weather >= 200 && weather <= 232) return 5;
    if(weather >= 300 && weather <= 321) return 2;
    if(weather >= 500 && weather <= 531) return 4;
    if(weather >= 600 && weather <= 622) return 4;
    if(weather >= 701 && weather <= 781) return 3;
    if(weather >= 782 && weather <= 800) return -4;
    if(weather == 801) return -4;
    if(weather == 802) return -3;
    if(weather == 803) return -2;
    if(weather == 804) return -1;
}

const getTempFactor = (temp) => {
    if(temp >= -10 && temp <= 10) return -1;
    if(temp >= 11 && temp <= 22) return -2;
    if(temp >= 23 && temp <= 35 ) return -3;

    return -4;
}

let stationsUpdated = 0;

const updateSensors = async () => {
    try {
        await sql.connect('mssql://joselyne:123@localhost/irrigacion2');
        const locations = await sql.query`SELECT id FROM Location`;
        locations.recordset.forEach(async (location, locationIndex) => {
            const stations = await sql.query`SELECT id, lat, lng, is_water_open FROM Station WHERE location_id = ${location.id}`;
            if(stations.recordset.length > 0 ){
                stations.recordset.forEach( async (station, index)=>{
                    const {lat, lng} = station;
                    let weather = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&APPID=${weather_appid}&units=metric`);
                    weather = await weather.json();

                    const hour = moment().tz(geoTz(lat, lng)[0]).format('HH');
                    let lastRecord = await sql.query`SELECT TOP 1 humidity, luminosity, temperature FROM Record WHERE station_id = ${station.id} ORDER BY captured_at DESC`;
                    
                    lastRecord = lastRecord.recordset[0] || {};
                    let humidity = lastRecord.humidity || 0;
                    if(station.is_water_open){
                        humidity = humidity + 20;
                    }else{
                        humidity = humidity - 2;
                    }
                    if(hour > 6 && hour < 18){
                        humidity = humidity - 3;
                    }else{
                        humidity = humidity -1;
                    }
                    humidity = humidity + getWeatherFactor(weather.weather[0].id);
                    humidity = humidity + getTempFactor(weather.main.temp);
                    await sql.query`INSERT INTO Record(station_id, humidity, luminosity, temperature, captured_at) VALUES (${station.id}, ${humidity}, ${weather.weather[0].id}, ${weather.main.temp}, CURRENT_TIMESTAMP)`;
                    console.log("SENSOR UPDATED", station.id, humidity, weather.weather[0].id, weather.main.temp) ;
                    stationsUpdated++;

                    const stations = await sql.query`SELECT id FROM Station`;
                    if(stationsUpdated === stations.recordset.length){
                        process.exit(1);
                    }
                   
                });
            }else{

            }
        });
        // process.exit(1);
    } catch (err) {
        console.log(err);
    }
}

/*
clima en funcion de la humedad
200-232: 5
300-321: 2
500-531: 4
600-622: 4
701-781: 3
782-800: -4
801: -4
802: -3
803: -2
804: -1
*/
const init = async() => {
    await updateSensors();

}




init();

//PARAMETROS DE ILUMINACION
/* 0 - 500 */ // OBSCURO
/*500 - 1000 */ // SEMICLARO
/*1000 - 2000 */ //CLARO